﻿using System;
using System.Collections;
using System.Collections.Generic;
using CaldasPack;
using CaldasPack.Prototyping;
using UnityEngine;

public class Main : MonoBehaviour
{
    private void Start()
    {
        CircularBuffer<int> cb = new CircularBuffer<int>(3);
        cb.Add(10);
        cb.Add(20);
        // cb.GetElementAt(0).LogError();
        // cb.GetElementAt(1).LogError();
        // cb.GetElementAt(2).LogError();
        // cb.GetElementAt(-1).LogError();
        // cb[0].LogError();
        // cb[1].LogError();
        // cb[2].LogError();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        ArrowDrawer.DrawGizmo(transform.position, Vector3.left);
        Gizmos.color = Color.cyan;
        ArrowDrawer.DrawGizmo(transform.position, Vector3.right);
        StringRenderer.Draw("This is the String Renderer called in OnDrawGizmos", Color.red, Vector3.zero, 0, -50);
    }


    void Update()
    {
        StringRenderer.Draw("This is the String Renderer called in Update", Color.green, Vector3.zero, 0, -75);

        ArrowDrawer.DrawDebug(transform.position, Vector3.up, Color.yellow);
        ArrowDrawer.DrawDebug(transform.position, Vector3.down, Color.magenta);
    }
}
